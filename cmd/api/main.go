package main

import (
	"airportProject/internal/dao"
	"airportProject/internal/entities"
	"airportProject/internal/utils"
	"encoding/json"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {
	redisConf := utils.GetRedisConfig()
	conn, err := redis.Dial(redisConf.Network, redisConf.Host+":"+redisConf.Port)
	if err != nil {
		os.Exit(-1)
	}
	defer conn.Close()

	r := mux.NewRouter()
	datesHandler := getMeasureBydates(conn)
	averageHandler := getAverageMeasure(conn)
	r.HandleFunc("/airport/{airportid}/{nature}/{start}/{end}", datesHandler).Methods("GET")
	r.HandleFunc("/airport/{airportid}/{date}", averageHandler).Methods("GET")

	http.Handle("/", r)
	apiConf := utils.GetApiConfig()
	fmt.Println("Api start on : http://" + apiConf.Host + ":" + apiConf.Port)
	http.ListenAndServe(apiConf.Host+":"+apiConf.Port, r)
}

func getMeasureBydates(conn redis.Conn) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		vars := mux.Vars(r)
		airportid, ok := getArgument(w, vars, "airportid")
		if !ok {
			return
		}

		nature, ok := getArgument(w, vars, "nature")
		if !ok {
			return
		}

		start, ok := getArgument(w, vars, "start")
		if !ok {
			return
		}

		end, ok := getArgument(w, vars, "end")
		if !ok {
			return
		}

		if !utils.Contain(utils.Airports, airportid) {
			sendError(w, "Argument \"airportid\" invalid")
			return
		}

		if !utils.Contain(utils.Natures, nature) {
			sendError(w, "Argument \"nature\" invalid")
			return
		}

		timeStart, ok := stringToTime(w, start)
		if !ok {
			return
		}

		timeEnd, ok := stringToTime(w, end)
		if !ok {
			return
		}

		dao := dao.NewMeasureDAO(conn)
		res, err := dao.Find(airportid, nature, timeStart, timeEnd)
		if err != nil {
			sendError(w, "Error when querying database")
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(ToJson(res))
	}
}

func getAverageMeasure(conn redis.Conn) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		vars := mux.Vars(r)

		airportid, ok := getArgument(w, vars, "airportid")
		if !ok {
			return
		}

		date, ok := getArgument(w, vars, "date")
		if !ok {
			return
		}

		start, err := time.Parse("02-01-2006", date)
		if err != nil {
			sendError(w, "Error while parsing date. Date format: DD-MM-YYYY")
			return
		}
		end := start.Add(24 * time.Hour)

		values := map[string]float64{}
		for _, nature := range utils.Natures {
			res, err := dao.NewMeasureDAO(conn).FindAverage(airportid, nature, start, end)
			if err != nil {
				sendError(w, "Error while getting data")
				return
			}
			values[utils.ResponseNature[nature]] = res
		}

		w.WriteHeader(http.StatusOK)
		resp, _ := json.Marshal(values)
		w.Write(resp)
	}
}

func ToJson(m []entities.Measure) []byte {
	resp, err := json.Marshal(m)
	if err != nil {
		panic("Error on marshalling array of Measure struct")
	}
	return resp
}

func sendError(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusBadRequest)
	resp := entities.NewError(msg)
	w.Write(resp.ToJson())
}

func getArgument(w http.ResponseWriter, vars map[string]string, name string) (string, bool) {
	value, ok := vars[name]
	if !ok {
		sendError(w, "Argument \""+name+"\" missing")
		return "", false
	}
	return value, true
}

func stringToTime(w http.ResponseWriter, str string) (time.Time, bool) {
	valueStr, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		sendError(w, "Error when parsing date")
		return time.Now(), false
	}
	return time.Unix(valueStr, 0), true
}
