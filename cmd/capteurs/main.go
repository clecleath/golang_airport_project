package main

import (
	captors "airportProject/internal/captors"
	dg "airportProject/internal/dataGenerator"
	"github.com/gomodule/redigo/redis"
	"os"
	"sync"
	"time"
)

var wg sync.WaitGroup

func initProject() {
	os.Mkdir("../data", 0777)
	os.Mkdir("../data/airports_csv_data", 0777)
	os.Mkdir("../data/airports_csv_data/cdg", 0777)
	os.Mkdir("../data/airports_csv_data/gnb", 0777)
	os.Mkdir("../data/airports_csv_data/nte", 0777)
	os.Mkdir("../data/airports_csv_data/pvg", 0777)

}

func main() {
	initProject()

	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		os.Exit(-1)
	}

	wg.Add(1)
	go dg.FileGenerator("tcp://localhost:1883", "fileGenerator", 0, "topic/test")

	wg.Add(1)
	go dg.RedisGenerator("tcp://localhost:1883", "RedisGenerator", 0, "topic/test", conn)
	time.Sleep(3 * time.Second)

	wg.Add(1)
	go captors.AirPressureCaptor("tcp://localhost:1883", "apcNTE1", "NTE", 1, "topic/test")
	wg.Add(1)
	go captors.AirPressureCaptor("tcp://localhost:1883", "apcNTE2", "NTE", 1, "topic/test")
	wg.Add(1)
	go captors.TemperatureCaptor("tcp://localhost:1883", "tempNTE1", "NTE", 1, "topic/test")
	wg.Add(1)
	go captors.WindSpeedCaptor("tcp://localhost:1883", "windNTE1", "NTE", 1, "topic/test")
	wg.Add(1)
	go captors.AirPressureCaptor("tcp://localhost:1883", "apcCDG1", "CDG", 1, "topic/test")
	wg.Add(1)
	go captors.TemperatureCaptor("tcp://localhost:1883", "tempCDG1", "CDG", 1, "topic/test")

	wg.Add(1)
	go captors.WindSpeedCaptor("tcp://localhost:1883", "windCDG1", "CDG", 1, "topic/test")

	//res, err := redis.Strings(conn.Do("zrevrange", "NTE:wind", 0, -1))
	//for i, re := range res {
	//	data := dg.FormatedData{}
	//	fmt.Println("(" + strconv.Itoa(i) + ") " + re)
	//	json.Unmarshal([]byte(re), &data)
	//	fmt.Println("\t" + data.CaptorId)
	//}
	////fmt.Printf("RES: r : %s (type %T) \n", res, res)
	wg.Wait()
	defer conn.Close()

}
