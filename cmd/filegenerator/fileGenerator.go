package main

import (
	utils "airportProject/internal/utils"
	"errors"
	"os"
	"sync"

	//"encoding/csv"

	"fmt"
	"strconv"
	"strings"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var wg sync.WaitGroup

func initProject() {
	os.Mkdir("airports_csv_data", 0777)
	os.Mkdir("airports_csv_data/cdg", 0777)
	os.Mkdir("airports_csv_data/gnb", 0777)
	os.Mkdir("airports_csv_data/nte", 0777)
	os.Mkdir("airports_csv_data/pvg", 0777)

}

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Need arguments [clientId] [topic]")
		os.Exit(-1)
	}
	
	initProject()
    
	captorConf := utils.GetCaptorConfig()
	clientId := os.Args[1]
	qos := captorConf.Qos
	topic := os.Args[2]

	wg.Add(1)
	FileGenerator(captorConf.BrokerURI, clientId, byte(qos), topic)
	wg.Wait()
}

func FileGenerator(brokerURI string, clientId string, qos byte, topic string) {
	client := utils.Connect(brokerURI, clientId)
	token := client.Subscribe(topic, qos, csvHandler)
	token.Wait()

}

// Used when we want to save data into a csv file of an aiport
var csvHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	//topic := msg.Topic()
	payload := msg.Payload()
	data := strings.Split(string(payload), ";")
	timestamp, _ := strconv.Atoi(data[0])
	date := time.Unix(int64(timestamp), 0)
	path := "airports_csv_data/" + strings.ToLower(data[2]) + "/" + strings.ToLower(data[2]) + "-" + date.Format("2006-01-02") + "-" + strings.ToLower(data[3]) + ".csv"
	if !exists(path) {
		os.Create(path)
	}
	file, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	if _, err := file.WriteString(string(payload) + "\n"); err != nil {
		fmt.Println(err)
	}

}

func exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}
