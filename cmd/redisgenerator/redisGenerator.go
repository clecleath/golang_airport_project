package main

import (
	"airportProject/internal/entities"
	utils "airportProject/internal/utils"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/gomodule/redigo/redis"
)

var wg sync.WaitGroup

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Need arguments [clientId] [topic]")
		os.Exit(-1)
	}
	captorConf := utils.GetCaptorConfig()
	redisConf := utils.GetRedisConfig()

	clientId := os.Args[1]
	qos := captorConf.Qos
	topic := os.Args[2]

	conn, err := redis.Dial(redisConf.Network, redisConf.Host+":"+redisConf.Port)
	if err != nil {
		os.Exit(-1)
	}

	wg.Add(1)
	RedisGenerator(captorConf.BrokerURI, clientId, byte(qos), topic, conn)
	wg.Wait()
	defer conn.Close()
}

func RedisGenerator(brokerURI string, clientId string, qos byte, topic string, conn redis.Conn) {
	client := utils.Connect(brokerURI, clientId)
	handler := redisHandler(conn)
	token := client.Subscribe(topic, qos, handler)
	token.Wait()
}

func redisHandler(conn redis.Conn) mqtt.MessageHandler {
	fun := "(redisGenerator/redisHandler)"
	return func(client mqtt.Client, msg mqtt.Message) {
		payload := msg.Payload()
		data := strings.Split(string(payload), ";")
		timestamp, _ := strconv.Atoi(data[0])
		value, _ := strconv.ParseFloat(data[4], 64)
		dt := &entities.Measure{
			SensorId:  data[1],
			AirportId: data[2],
			Nature:    data[3],
			Value:     value,
			Date:      uint64(timestamp),
		}
		jsonData, err := json.Marshal(dt)
		if err != nil {
			fmt.Println("Error on data marshalling: " + fun)
			os.Exit(-1)
		}

		_, err = conn.Do("ZADD", data[2]+":"+data[3], timestamp, jsonData)
		if err != nil {
			os.Exit(-1)
		}

	}
}
