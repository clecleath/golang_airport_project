package main

import (
	"airportProject/internal/dao"
	"airportProject/internal/entities"
	"airportProject/internal/utils"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"html/template"
	"log"
	"net/http"
	"strings"
	"time"
)

var dbConn redis.Conn
var baseUrl = "internal/htmlFiles/"

func homeHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, baseUrl+"index.html")
}

func measuresListHandler(w http.ResponseWriter, r *http.Request) {
	parameters := r.URL.Query()
	airport := parameters.Get("airport-id")
	startText := parameters.Get("start-date") + " " + parameters.Get("start-hour")
	endText := parameters.Get("end-date") + " " + parameters.Get("end-hour")
	start, errStart := time.ParseInLocation("2006-01-02 15:04", startText, time.Local)
	end, errEnd := time.ParseInLocation("2006-01-02 15:04", endText, time.Local)

	var values struct {
		Measures  []entities.StringFormatedMeasure
		AirportId string
		StartDate string
		StartHour string
		EndDate   string
		EndHour   string
		Pressure  bool
		Temp      bool
		Wind      bool
	}
	
	values.Pressure = false
	values.Temp = false
	values.Wind = false

	var selectedNatures []string
	if len(parameters["categories"]) == 0 {
		selectedNatures = utils.Natures
	} else {
		selectedNatures = parameters["categories"]
	}

	if errStart == nil && errEnd == nil {
		for _, nature := range selectedNatures {
			data, err := dao.NewMeasureDAO(dbConn).Find(airport, nature, start, end)
			if err == nil {

				for _, entity := range data {
					values.Measures = append(values.Measures, entity.StringFormat())
				}

				switch nature {
					case "pressure":
						values.Pressure = true
						break
					case "temp":
						values.Temp = true
						break
					case "wind":
						values.Wind = true
						break
					}
			} else {
				log.Panic(err)
			}
		}
	}

	values.AirportId = airport
	values.StartDate = or(parameters.Get("start-date"), time.Now().Format("2006-01-02"))
	values.StartHour = or(parameters.Get("start-hour"), "00:00")
	values.EndDate = or(parameters.Get("end-date"), time.Now().AddDate(0, 0, 1).Format("2006-01-02"))
	values.EndHour = or(parameters.Get("end-hour"), "00:00")

	tmpl, err := template.New("measures-list.html").ParseFiles(baseUrl + "measures-list.html")
	err = tmpl.Execute(w, values)
	if err != nil {
		log.Panic(err)
	}
}

func averageMeasureHandler(w http.ResponseWriter, r *http.Request) {
	values := make(map[string]string)

	parameters := r.URL.Query()
	airport := parameters.Get("airport-id")
	start, err := time.ParseInLocation("2006-01-02", parameters.Get("date"), time.Local)
	if err == nil {
		end := start.AddDate(0, 0, 1)

		for _, nature := range utils.Natures {
			average, err := dao.NewMeasureDAO(dbConn).FindAverage(airport, nature, start, end)
			if err == nil {
				values[strings.Title(nature)] = fmt.Sprintf("%.2f %s", average, utils.Units[nature])
			} else {
				log.Panic(err)
			}
		}

		values["Date"] = start.Format("2006-01-02")
	} else {
		values["Date"] = time.Now().Format("2006-01-02")
	}

	values["AirportId"] = airport
	tmpl, err := template.New("average-measure.html").ParseFiles(baseUrl + "average-measure.html")
	err = tmpl.Execute(w, values)
	if err != nil {
		log.Panic(err)
	}
}

func or(a string, b string) string {
	if len(a) > 0 {
		return a
	} else {
		return b
	}
}

func main() {
	var err error
	redisConf := utils.GetRedisConfig()
	dbConn, err = redis.Dial(redisConf.Network, redisConf.Host+":"+redisConf.Port)
	if err != nil {
		panic(err)
	}
	defer dbConn.Close()

	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/home", homeHandler)
	http.HandleFunc("/measures-list", measuresListHandler)
	http.HandleFunc("/average-measure", averageMeasureHandler)
	// Useful for stylesheet
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir(baseUrl+"/assets"))))

	webapp := utils.GetWebappConfig()
	fmt.Println("Webapp start on : http://" + webapp.Host + ":" + webapp.Port)
	err = http.ListenAndServe(webapp.Host+":"+webapp.Port, nil)
	if err != nil {
		panic(err)
	}
}
