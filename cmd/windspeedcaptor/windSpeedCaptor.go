package main

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	utils "airportProject/internal/utils"
)

func main() {
	if len(os.Args) < 4 {
		log.Println("Need arguments [clientId] [aiportId] [topic]")
		os.Exit(-1)
	}

	captorConf := utils.GetCaptorConfig()
	clientId := os.Args[1]
	airportId := os.Args[2]
	qos := captorConf.Qos
	topic := os.Args[3]
	durationSleep := captorConf.DurationSleep

	WindSpeedCaptor(captorConf.BrokerURI, clientId, airportId, byte(qos), topic, durationSleep)
}

func WindSpeedCaptor(brokerURI string, clientId string, airportId string, qos byte, topic string, durationSleep int) {
	client := utils.Connect(brokerURI, clientId)
	// Infinite loop wich send air pressure data to the mqtt brocker
	rank := 1
	csvFile, err := os.Open("./data/" + strings.ToLower(airportId) + "/" + strings.ToLower(airportId) + "-wind.csv")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Successfully Opened CSV file")
	defer csvFile.Close()
	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		log.Println(err)
	}
	for {
		line := csvLines[rank]
		datum := strings.Split(line[0], ";")
		data := strconv.FormatInt(time.Now().Unix(), 10) + ";" + clientId + ";" + airportId + ";wind;" + datum[1]
		token := client.Publish(topic, qos, false, data)
		token.Wait()
		time.Sleep(time.Duration(durationSleep) * time.Second)
		rank++
	}
}
