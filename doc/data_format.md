# Data format   

## CSV format from captor
 

| timestamp       |    IdCaptor     |  IdAirport  | mesureType  | value |
| :-------------: | :-------------: | :---------: | :---------: | :---: |
| 1639318906      |        30       |     NTE     |    wind     |  25.6 |
| 1639318916      |        30       |     NTE     |    wind     |  25.2 |  
| 1639318916      |        59       |     GNB     | temperature |  18.9 |
| 1639318926      |        19       |     PVG     |   pressure  |  1.7  |

Content of the MQTT publication by the captor : 30

    1639318906;30;NTE;wind;25.6

## data format for redis

Sorted Set
clé: IATA:mesureType
score: timestamp
valeur: {idCaptor: "", value: }