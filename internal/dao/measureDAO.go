package dao

import (
	"airportProject/internal/entities"
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	"time"
)

type MeasureDAO struct {
	conn redis.Conn
}

func NewMeasureDAO(conn redis.Conn) MeasureDAO {
	return MeasureDAO{conn}
}

func (dao MeasureDAO) Find(airport string, nature string, start time.Time, end time.Time) ([]entities.Measure, error) {
	var measures []entities.Measure

	data, err := redis.Strings(dao.conn.Do("ZRANGEBYSCORE", airport+":"+nature, start.Unix(), end.Unix()))
	if err != nil {
		return measures, err
	}

	measures = arrayToStruc(data)

	return measures, nil
}

func (dao MeasureDAO) FindAverage(airport string, nature string, start time.Time, end time.Time) (float64, error) {
	var measures []entities.Measure

	data, err := redis.Strings(dao.conn.Do("ZRANGEBYSCORE", airport+":"+nature, start.Unix(), end.Unix()))
	if err != nil {
		return 0.0, err
	}

	measures = arrayToStruc(data)
	sum := 0.0
	for _, measure := range measures {
		sum += measure.Value
	}

	return (sum) / (float64(len(measures))), nil
}

func arrayToStruc(str []string) []entities.Measure {
	var res []entities.Measure
	for _, re := range str {
		data := entities.Measure{}
		json.Unmarshal([]byte(re), &data)
		res = append(res, data)
	}
	return res
}
