package entities

import "encoding/json"

type Error struct {
	Message string
}

func NewError(msg string) Error {
	return Error{Message: msg}
}

func (e Error) String() string {
	return e.Message
}

func (e Error) ToJson() []byte {
	resp, err := json.Marshal(e)
	if err != nil {
		panic("Error on marshalling Error struct")
	}
	return resp
}
