package entities

import (
	"airportProject/internal/utils"
	"encoding/json"
	"fmt"
	"time"
)

var natureLabels = map[string]string{
	"pressure": "Pressure",
	"temp":     "Temperature",
	"wind":     "Wind",
}

type Measure struct {
	SensorId  string
	AirportId string
	Nature    string
	Value     float64
	Date      uint64
}

type FormatedMeasure struct {
	SensorId  string
	AirportId string
	Nature    string
	Value     float64
	Date      string
}

type StringFormatedMeasure struct {
    SensorId    string
    AirportId   string
    Nature      string
    Value       string
    Date        string
}

func NewMeasure() Measure {
	return Measure{}
}

func (m Measure) String() string {
	return fmt.Sprintf("%s %s %f", m.Date, m.Value, m.SensorId)
}

func (m Measure) Format() FormatedMeasure {
	res := FormatedMeasure{
		SensorId:  m.SensorId,
		Nature:    m.Nature,
		Value:     m.Value,
		AirportId: m.AirportId,
	}

	dt := time.Unix(int64(m.Date), 0)
	res.Date = dt.Format("02/01/2006 15:04:05")

	return res
}

func (m Measure) StringFormat() StringFormatedMeasure {
	fmtMeasure := m.Format()
	
	return StringFormatedMeasure {
		SensorId:   fmtMeasure.SensorId,
		Nature:     fmtMeasure.Nature,
		Value:      fmt.Sprintf("%.2f %s", fmtMeasure.Value, utils.Units[fmtMeasure.Nature]),
		AirportId:  fmtMeasure.AirportId,
		Date:       fmtMeasure.Date,
	}
}

func (m Measure) ToJson() []byte {
	resp, err := json.Marshal(m)
	if err != nil {
		panic("Error on marshalling Measure struct")
	}
	return resp
}
