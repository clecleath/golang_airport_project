package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type ApiType struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type WebappType struct {
	Host string `json:"host"`
	Port string `json:"port"`
}

type RedisType struct {
	Host    string `json:"host"`
	Port    string `json:"port"`
	Network string `json:"network"`
}

type CaptorType struct {
	Qos           int    `json:"qos"`
	DurationSleep int    `json:"durationSleep"`
	BrokerURI     string `json:"brokerURI"`
}

type Config struct {
	Api    ApiType    `json:"api"`
	Webapp WebappType `json:"webapp"`
	Redis  RedisType  `json:"redis"`
	Captor CaptorType `json:"captor"`
}

func getConfigs() Config {
	jsonFile, err := os.Open("config.json")
	if err != nil {
		fmt.Println(err)
		panic("Config file not found")
	}
	defer jsonFile.Close()

	jsonData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		panic("Cannot read config file")
	}

	conf := Config{}
	err = json.Unmarshal(jsonData, &conf)
	if err != nil {
		panic("Failed to Unmarshall config")
	}

	return conf
}

func GetApiConfig() ApiType {
	return getConfigs().Api
}

func GetWebappConfig() WebappType {
	return getConfigs().Webapp
}

func GetRedisConfig() RedisType {
	return getConfigs().Redis
}

func GetCaptorConfig() CaptorType {
	return getConfigs().Captor
}
