package utils

var Airports = []string{"CDG", "GNB", "NTE"}
var Natures = []string{"pressure", "temp", "wind"}
var Units = map[string]string{"pressure": "hPa", "temp": "°C", "wind": "km/h"}
var ResponseNature = map[string]string{"pressure": "athmosphericPressure", "temp": "temperature", "wind": "windSpeed"}

func Contain(arr []string, value string) bool {
	for _, s := range arr {
		if s == value {
			return true
		}
	}
	return false
}
